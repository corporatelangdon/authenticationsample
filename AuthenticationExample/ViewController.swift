//
//  ViewController.swift
//  AuthenticationExample
//
//  Created by James Langdon on 10/5/15.
//  Copyright © 2015 James Langdon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    var countDown = 5
    var timer: NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up observer for loggedIn property of authenticateUser object.  When value changes, text is updated via callback
        authenticateUser.loggedIn.subscribe() {oldValue, newValue in
            if newValue {
                self.messageLabel.text = "User is logged in"
            } else {
                self.messageLabel.text = "User is logged out"
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loginButton(sender: UIButton) {
        authenticateUser.login(usernameTextField.text!, password: passwordTextField.text!)
        startTimer()
    }

    // Simple timer
    func startTimer() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "updateTimerLabel", userInfo: nil, repeats: true)
    }
    
    func updateTimerLabel() {
        if countDown >= 0 {
            timerLabel.text = "User will be logged out in \(countDown.description) seconds."
            countDown--
        } else {
            resetTimer()
        }
    }
    
    // Logs user off resets timer value
    func resetTimer() {
        timer?.invalidate()
        authenticateUser.logout()
        countDown = 5
    }
    
    @IBAction func logoutButton(sender: UIButton) {
        resetTimer()
    }
}

