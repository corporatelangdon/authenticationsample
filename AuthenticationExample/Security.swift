//
//  Security.swift
//  AuthenticationExample
//
//  Created by James Langdon on 10/5/15.
//  Copyright © 2015 James Langdon. All rights reserved.
//

import Foundation

let authenticateUser = Security()

//Simple data structure for users and pwds
class Credential {
    static let credentialStore = ["james":"kvoexpert","ransom":"buckcluck","u":"p"]
}

// Class prepped for singleton usage. Subclasses not allowed, can't be instantiated outside of Security.swift file
final class Security: NSObject {
    private var username: String?
    private var password: String?
    
    // Value wrapped in Obervable struct. Means this value can be monitored by other objects that subscribe to it
    var loggedIn = Observable(false)
    
    private override init() {}
    
    func login(username: String, password: String) -> String {
        var msg = ""
        if validateCredentials(username, password: password){
            self.username = username
            self.password = password
            
            // Because loggedIn property is wrapped in Obersvable, must use "value" property of wrapper to extract actual value
            self.loggedIn.value = true
            msg = "User Logged In Successfully"
        } else {
            msg = "Invalid Credentials"
        }
        return msg
    }
    
    func validateCredentials(username: String, password: String) -> Bool {
        var validCredential = false
        for (user, pass) in Credential.credentialStore {
            if username == user && password == pass{
                validCredential = true
            }
        }
        return validCredential
    }
    
    func logout() {
        self.username = nil
        self.password = nil
        loggedIn.value = false
    }
    
}

