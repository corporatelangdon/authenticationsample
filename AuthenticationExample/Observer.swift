//
//  Observer.swift
//  AuthenticationExample
//
//  Created by James Langdon on 10/5/15.
//  Copyright © 2015 James Langdon. All rights reserved.
//

import Foundation

// Wraps property and adds it to Subscriptions collection for observation via suscribe method. Uses generics to bypass type contraints
struct Observable<T> {
    typealias ValueType = T
    typealias SubscriptionType = Subscription<T>
    
    var value: ValueType {
        didSet {
            for subscription in subscriptions {
                let event = ValueChangeEvent(oldValue, value)
                subscription.notify(event)
            }
        }
    }
    
    private var subscriptions = Subscriptions<ValueType>()
    
    mutating func subscribe(handler: SubscriptionType.HandlerType) -> SubscriptionType {
        return subscriptions.add(handler)
    }
    
    mutating func unsubscribe(subscription: SubscriptionType) {
        subscriptions.remove(subscription)
    }
    
    init(_ value: ValueType) {
        self.value = value
    }
}

// Simple structure contains old and new values for observed property
struct ValueChangeEvent<T> {
    typealias ValueType = T
    
    let oldValue: ValueType
    let newValue: ValueType
    
    init(_ o: ValueType, _ n: ValueType) {
        oldValue = o
        newValue = n
    }
}

// Handles callback for value change
class Subscription<T> {
    typealias ValueType = T
    typealias HandlerType = (oldValue: ValueType, newValue: ValueType) -> ()
    
    let handler: HandlerType
    
    init(handler: HandlerType) {
        self.handler = handler
    }
    
    func notify(event: ValueChangeEvent<ValueType>) {
        self.handler(oldValue: event.oldValue, newValue: event.newValue)
    }
}

// Collection for subscription objects
struct Subscriptions<T>: SequenceType {
    typealias ValueType = T
    typealias SubscriptionType = Subscription<T>
    
    private var subscriptions = [SubscriptionType]()
    
    mutating func add(handler: SubscriptionType.HandlerType) -> SubscriptionType {
        let subscription = Subscription(handler: handler)
        self.subscriptions.append(subscription)
        return subscription
    }
    
    mutating func remove(subscription: SubscriptionType) {
        var index = NSNotFound
        var i = 0
        for element in self.subscriptions as [SubscriptionType] {
            if element === subscription {
                index = i
                break
            }
            i++
        }
        
        if index != NSNotFound {
            self.subscriptions.removeAtIndex(index)
        }
    }
    
    // MARK: SequenceType protocol
    
    // Allows Subscriptions structure to be iterated over via for loop
    func generate() -> AnyGenerator<Subscription<ValueType>> {
        var nextIndex = subscriptions.count-1
        return anyGenerator {
            if nextIndex < 0 {
                return nil
            }
            return self.subscriptions[nextIndex--]
        }
    }
    
}